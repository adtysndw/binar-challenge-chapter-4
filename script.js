let batu = document.getElementById("batu");
let kertas = document.getElementById("kertas");
let gunting = document.getElementById("gunting");
let infoHasil = document.getElementById("info-hasil");
let tombolReset = document.getElementById("reset");
let aktifKelas = document.getElementsByClassName("alert-dark");
let alat = ["batu", "kertas", "gunting"];
let teksAwal = 'VS';
let compScore = document.getElementById("comp-score");
let playerScore = document.getElementById("player-score");


tombolReset.addEventListener("click", function () {
    reset();
})

function reset() {
    infoHasil.textContent = teksAwal;
    kertas.classList.remove("alert-dark");
    gunting.classList.remove("alert-dark");
    batu.classList.remove("alert-dark");

    alat.forEach((item) => {
        let pilihanCompHapusElemen = document.getElementById(`${item}-comp`);
        pilihanCompHapusElemen.classList.remove("alert-dark");
    })
}

batu.addEventListener("click", function (e) {
    if (aktifKelas.length < 2) {
        batu.classList.add("alert-dark");
        kertas.classList.remove("alert-dark");
        gunting.classList.remove("alert-dark");
        main('batu');
    }
})

kertas.addEventListener("click", function (e) {
    if (aktifKelas.length < 2) {
        kertas.classList.add("alert-dark");
        batu.classList.remove("alert-dark");
        gunting.classList.remove("alert-dark");
        main('kertas');
    }
})

gunting.addEventListener("click", function (e) {
    if (aktifKelas.length < 2) {
        gunting.classList.add("alert-dark");
        batu.classList.remove("alert-dark");
        kertas.classList.remove("alert-dark");
        main('gunting');
    }
})

function main(pilihan) {
    if (aktifKelas.length < 2) {
        let pilihanPlayer = pilihan;

        let nomorAcak = Math.floor(Math.random() * 3);
        let pilihanComp = alat[nomorAcak];

        alat.forEach((item) => {
            let pilihanCompHapusElemen = document.getElementById(`${item}-comp`);
            pilihanCompHapusElemen.classList.remove("alert-dark");
        })

        let pilihanCompElemen = document.getElementById(`${pilihanComp}-comp`);
        pilihanCompElemen.classList.add("alert-dark");

        let hasil = cekMenang(pilihanPlayer, pilihanComp);

        if (hasil === 'COMP WIN') {
            let textCompScore = parseInt(compScore.textContent);
            compScore.textContent = textCompScore + 1;
        }

        if (hasil === 'PLAYER WIN') {
            let textPlayerScore = parseInt(playerScore.textContent);
            playerScore.textContent = textPlayerScore + 1;
        }

        infoHasil.textContent = hasil;

    }
}

function cekMenang(pilihanPlayer, pilihanComp) {
    if (pilihanPlayer === pilihanComp) return 'DRAW';
    if (pilihanPlayer === 'batu' && pilihanComp === 'kertas') {
        return 'COMP WIN';
    }
    if (pilihanPlayer === 'batu' && pilihanComp === 'gunting') {
        return 'PLAYER WIN';
    }
    if (pilihanPlayer === 'kertas' && pilihanComp === 'batu') {
        return 'PLAYER WIN';
    }
    if (pilihanPlayer === 'kertas' && pilihanComp === 'gunting') {
        return 'COMP WIN';
    }
    if (pilihanPlayer === 'gunting' && pilihanComp === 'batu') {
        return 'COMP WIN';
    }
    if (pilihanPlayer === 'gunting' && pilihanComp === 'kertas') {
        return 'PLAYER WIN';
    }
}
